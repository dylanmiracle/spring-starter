# CI Test Project


## Maven
```
  script:
    - 'mvn versions:update-parent -U -Dmaven.repo.local=maven.repository'
    - 'mvn versions:use-latest-versions -U -Dmaven.repo.local=maven.repository'
    - 'mvn versions:set -DnewVersion=0.0.3-SNAPSHOT'
    - 'mvn versions:commit'
    - 'mvn clean verify'
    - 'mvn deploy -s ci_settings.xml'
```
Initialized empty Git repository in /builds/dylanmiracle/spring-starter/.git/

## Docker

## To Do
- Auto-increment pom (possibly use maven release plugin)
- subdirectories / mono-repo / only builds what changes
- Rules to skip build, commit nonbuilding changes