FROM openjdk:11-jdk

COPY ./target/*.jar /usr/local/app/app.jar

ENTRYPOINT ["java", "-jar", "/usr/local/app/app.jar"]